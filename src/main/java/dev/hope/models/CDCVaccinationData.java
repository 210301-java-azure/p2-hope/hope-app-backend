package dev.hope.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class CDCVaccinationData {
    @JsonProperty("Date")
    private String date;
    @JsonProperty("Location")
    private String location;
    @JsonProperty("LongName")
    private String longName;

    public CDCVaccinationData() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String Date) {
        this.date = Date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String Location) {
        this.location = Location;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CDCVaccinationData that = (CDCVaccinationData) o;
        return Objects.equals(date, that.date) && Objects.equals(location, that.location) && Objects.equals(longName, that.longName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, location, longName);
    }

    @Override
    public String toString() {
        return "CDCVaccinationData{" +
                "date='" + date + '\'' +
                ", location='" + location + '\'' +
                ", longName='" + longName + '\'' +
                '}';
    }
}
